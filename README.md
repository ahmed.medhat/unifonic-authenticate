# Getting started

Handles verifications with OTP to multiple channels

## How to Build

The generated code has dependencies over external libraries like UniRest. These dependencies are defined in the ```composer.json``` file that comes with the SDK. 
To resolve these dependencies, we use the Composer package manager which requires PHP greater than 5.3.2 installed in your system. 
Visit [https://getcomposer.org/download/](https://getcomposer.org/download/) to download the installer file for Composer and run it in your system. 
Open command prompt and type ```composer --version```. This should display the current version of the Composer installed if the installation was successful.

* Using command line, navigate to the directory containing the generated files (including ```composer.json```) for the SDK. 
* Run the command ```composer install```. This should install all the required dependencies and create the ```vendor``` directory in your project directory.

![Building SDK - Step 1](https://apidocs.io/illustration/php?step=installDependencies&workspaceFolder=Unifonic%20Authenticate-PHP)

### [For Windows Users Only] Configuring CURL Certificate Path in php.ini

CURL used to include a list of accepted CAs, but no longer bundles ANY CA certs. So by default it will reject all SSL certificates as unverifiable. You will have to get your CA's cert and point curl at it. The steps are as follows:

1. Download the certificate bundle (.pem file) from [https://curl.haxx.se/docs/caextract.html](https://curl.haxx.se/docs/caextract.html) on to your system.
2. Add curl.cainfo = "PATH_TO/cacert.pem" to your php.ini file located in your php installation. “PATH_TO” must be an absolute path containing the .pem file.

```ini
[curl]
; A default value for the CURLOPT_CAINFO option. This is required to be an
; absolute path.
;curl.cainfo =
```

## How to Use

The following section explains how to use the UnifonicAuthenticate library in a new project.

### 1. Open Project in an IDE

Open an IDE for PHP like PhpStorm. The basic workflow presented here is also applicable if you prefer using a different editor or IDE.

![Open project in PHPStorm - Step 1](https://apidocs.io/illustration/php?step=openIDE&workspaceFolder=Unifonic%20Authenticate-PHP)

Click on ```Open``` in PhpStorm to browse to your generated SDK directory and then click ```OK```.

![Open project in PHPStorm - Step 2](https://apidocs.io/illustration/php?step=openProject0&workspaceFolder=Unifonic%20Authenticate-PHP)     

### 2. Add a new Test Project

Create a new directory by right clicking on the solution name as shown below:

![Add a new project in PHPStorm - Step 1](https://apidocs.io/illustration/php?step=createDirectory&workspaceFolder=Unifonic%20Authenticate-PHP)

Name the directory as "test"

![Add a new project in PHPStorm - Step 2](https://apidocs.io/illustration/php?step=nameDirectory&workspaceFolder=Unifonic%20Authenticate-PHP)
   
Add a PHP file to this project

![Add a new project in PHPStorm - Step 3](https://apidocs.io/illustration/php?step=createFile&workspaceFolder=Unifonic%20Authenticate-PHP)

Name it "testSDK"

![Add a new project in PHPStorm - Step 4](https://apidocs.io/illustration/php?step=nameFile&workspaceFolder=Unifonic%20Authenticate-PHP)

Depending on your project setup, you might need to include composer's autoloader in your PHP code to enable auto loading of classes.

```PHP
require_once "../vendor/autoload.php";
```

It is important that the path inside require_once correctly points to the file ```autoload.php``` inside the vendor directory created during dependency installations.

![Add a new project in PHPStorm - Step 4](https://apidocs.io/illustration/php?step=projectFiles&workspaceFolder=Unifonic%20Authenticate-PHP)

After this you can add code to initialize the client library and acquire the instance of a Controller class. Sample code to initialize the client library and using controller methods is given in the subsequent sections.

### 3. Run the Test Project

To run your project you must set the Interpreter for your project. Interpreter is the PHP engine installed on your computer.

Open ```Settings``` from ```File``` menu.

![Run Test Project - Step 1](https://apidocs.io/illustration/php?step=openSettings&workspaceFolder=Unifonic%20Authenticate-PHP)

Select ```PHP``` from within ```Languages & Frameworks```

![Run Test Project - Step 2](https://apidocs.io/illustration/php?step=setInterpreter0&workspaceFolder=Unifonic%20Authenticate-PHP)

Browse for Interpreters near the ```Interpreter``` option and choose your interpreter.

![Run Test Project - Step 3](https://apidocs.io/illustration/php?step=setInterpreter1&workspaceFolder=Unifonic%20Authenticate-PHP)

Once the interpreter is selected, click ```OK```

![Run Test Project - Step 4](https://apidocs.io/illustration/php?step=setInterpreter2&workspaceFolder=Unifonic%20Authenticate-PHP)

To run your project, right click on your PHP file inside your Test project and click on ```Run```

![Run Test Project - Step 5](https://apidocs.io/illustration/php?step=runProject&workspaceFolder=Unifonic%20Authenticate-PHP)

## How to Test

Unit tests in this SDK can be run using PHPUnit. 

1. First install the dependencies using composer including the `require-dev` dependencies.
2. Run `vendor\bin\phpunit --verbose` from commandline to execute tests. If you have 
   installed PHPUnit globally, run tests using `phpunit --verbose` instead.

You can change the PHPUnit test configuration in the `phpunit.xml` file.

## Initialization

### Authentication
In order to setup authentication and initialization of the API client, you need the following information.

| Parameter | Description |
|-----------|-------------|
| xAuthenticateAppId | TODO: add a description |
| authorization | Bearer Auth Token |



API client can be initialized as following.

```php
$xAuthenticateAppId = 'xAuthenticateAppId';
$authorization = 'Bearer AUTH_TOKEN'; // Bearer Auth Token

$client = new UnifonicAuthenticateLib\UnifonicAuthenticateClient($xAuthenticateAppId, $authorization);
```


# Class Reference

## <a name="list_of_controllers"></a>List of Controllers

* [VerificationsController](#verifications_controller)

## <a name="verifications_controller"></a>![Class: ](https://apidocs.io/img/class.png ".VerificationsController") VerificationsController

### Get singleton instance

The singleton instance of the ``` VerificationsController ``` class can be accessed from the API Client.

```php
$verifications = $client->getVerifications();
```

### <a name="create_check_verification"></a>![Method: ](https://apidocs.io/img/method.png ".VerificationsController.createCheckVerification") createCheckVerification

> TODO: Add a method description


```php
function createCheckVerification(
        $to,
        $channel,
        $code)
```

#### Parameters

| Parameter | Tags | Description |
|-----------|------|-------------|
| to |  ``` Required ```  | Address of receiver |
| channel |  ``` Required ```  | Channel you want to send. Possible values are `sms`, `whatsapp` & `voice`. |
| code |  ``` Required ```  | The code which user has entered |



#### Example Usage

```php
$to = 'to';
$channel = 'channel';
$code = 'code';

$result = $verifications->createCheckVerification($to, $channel, $code);

```

#### Errors

| Error Code | Error Description |
|------------|-------------------|
| 400 | bad input parameter |



### <a name="create_verification"></a>![Method: ](https://apidocs.io/img/method.png ".VerificationsController.createVerification") createVerification

> This endpoint sends a verification code through any of channels mainly `Sms`, `Voice` & `Whatsapp`. By default it sends messages to all configured channels in the Authenticate app.


```php
function createVerification(
        $to,
        $channel = null,
        $locale = 'en',
        $length = null)
```

#### Parameters

| Parameter | Tags | Description |
|-----------|------|-------------|
| to |  ``` Required ```  | Address of receiver |
| channel |  ``` Optional ```  | Channel you want to use for sending verification code. Currently there are three channels  `SMS`, `Voice` & `Whatsapp`. Their priorities can be defined in Unifonic Authenticate configuration panel. By default it will send verification through all of configured channels. If channel is not defined the verification code will be sent on the basis of priority defined for each channel. |
| locale |  ``` Optional ```  ``` DefaultValue ```  | In what language you want to send |
| length |  ``` Optional ```  | Length of verification code |



#### Example Usage

```php
$to = 'to';
$channel = 'channel';
$locale = 'en';
$length = 'length';

$result = $verifications->createVerification($to, $channel, $locale, $length);

```

#### Errors

| Error Code | Error Description |
|------------|-------------------|
| 400 | bad input parameter |



[Back to List of Controllers](#list_of_controllers)



